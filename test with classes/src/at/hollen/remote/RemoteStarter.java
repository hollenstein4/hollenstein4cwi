package at.hollen.remote;

public class RemoteStarter {

	public static void main(String[] args) {
		Device d1 = new Device("12345ab", Device.type.television);
		Device d2 = new Device("23456bc", Device.type.rollo);
		
		Remote r1 = new Remote(70, 20, 250, "1234");
		
		r1.addDevices(d1);
		r1.addDevices(d2);
		
		Remote r2 = new Remote(60, 15, 100, "2345");
		Remote r3 = r2;
		
		r3.getSerialNumber();
		
		
	}

}
