package at.hollen.remote;

import java.util.ArrayList;
import java.util.List;

public class Remote {

	private int length;
	private int width;
	private int weight; // in g
	private String serialNumber;
	private boolean isOn = false;
	private List<Device> devices;

	public Remote(int length, int width, int weight, String serialNumber) {
		super();
		this.length = length;
		this.width = width;
		this.weight = weight;
		this.serialNumber = serialNumber;
		this.devices = new ArrayList<>();
	}
	
	public void addDevices(Device device) {
		this.devices.add(device);
	}
	
	public void turnOn() {
		System.out.println("i am turning on now");
	}
	public void turnOff() {
		System.out.println("i am turning off now");
	}
	public void tellSerial() {
		System.out.println("My Serial Number is " + this.serialNumber);
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public boolean isOn() {
		return isOn;
	}

	public void setOn(boolean isOn) {
		this.isOn = isOn;
	}

	
}