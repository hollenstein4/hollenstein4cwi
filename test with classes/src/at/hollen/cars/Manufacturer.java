package at.hollen.cars;

public class Manufacturer {
	public String name;
	public String country;
	public int discount; //in percent
	public String fuelType;
	public int performance;
	
	
	public Manufacturer(String name, String country, int discount, String fuelType, int performance) {
		super();
		this.name = name;
		this.country = country;
		this.discount = discount; //in percent
		this.fuelType = fuelType;
		this.performance = performance;
	}


	public String getName() {
		return name;
	}


	public String getCountry() {
		return country;
	}


	public int getDiscount() {
		return discount;
	}


	public String getFuelType() {
		return fuelType;
	}


	public int getPerformance() {
		return performance;
	}


	
}