package at.hollen.cars;

public class Car {
	public static double CONSUMPTION = 1.098; //for mileage over 50.000km
	
	public String color;
	public int maxSpeed; //in kmh
	public int basisPrice;
	public double basisConsumption;
	public int mileage; //in km
	public Manufacturer manufacturer;
	public Engine engine;
	
	
	public Car(String color, int maxSpeed, int basisPrice, double basisConsumption, Manufacturer manufacturer, Engine engine) {
		this.color = color;
		this.maxSpeed = maxSpeed;
		this.basisPrice = basisPrice;
		this.basisConsumption = basisConsumption;
		this.manufacturer = manufacturer;
		this.engine = engine;
	}
	
	public int getRetailPrice() {
		System.out.println("Price considering discount:");
		return (this.basisPrice - (this.basisPrice / 100 * manufacturer.discount));
	}

	public String getColor() {
		return color;
	}

	public int getMaxSpeed() {
		return maxSpeed;
	}

	public int getBasisPrice() {
		System.out.println("Price without discount:");
		return basisPrice;
	}

	public double getBasisConsumption() {
		if(mileage < 50000) {
			return basisConsumption;
		} else {
			return basisConsumption * CONSUMPTION;
		}
	}

	public Manufacturer getManufracturer() {
		return manufacturer;
	}

	public Engine getEngine() {
		return engine;
	}

	public int getMileage() {
		return mileage;
	}

	public void setMileage(int mileage) {
		this.mileage = mileage;
	}
	
	
	
	
	
}