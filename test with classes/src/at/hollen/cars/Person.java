package at.hollen.cars;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Person {
	private String fName;
	private String lName;
	private LocalDate birthday;
	private List<Car> cars;
	
	public Person(String fName, String lName, LocalDate birthday) {
		super();
		this.fName = fName;
		this.lName = lName;
		this.birthday = birthday;
		this.cars = new ArrayList<>();
	}

	public void addCars(Car cars) {
		this.cars.add(cars);
	}
	
	
	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public LocalDate getBirthday() {
		return birthday;
	}

	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}

	public List<Car> getCars() {
		return cars;
	}

	public void setCars(List<Car> cars) {
		this.cars = cars;
	}
	
	public int getValueOfCars() {
		int amount = 0;
		for (Car cars : this.cars) {
			amount = amount + cars.getRetailPrice();			
		}
		return amount;
	}
	
}

