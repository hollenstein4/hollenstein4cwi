package at.hollen.cars;

import java.time.LocalDate;

public class mainCar {

	public static void main(String[] args) {
		
		
		Person Person1 = new Person("Jonas", "Hollenstein", LocalDate.of(2000,  04, 04));
		Engine A170 = new Engine("Diesel", 110);
		Manufacturer Mercedes = new Manufacturer("Mercedes", "Germany", 3, "Diesel", 130);
		Car AClass = new Car("red", 220, 20000, 4.3, Mercedes, A170);

		AClass.setMileage(30000);
		AClass.getRetailPrice();
		
		Person1.getValueOfCars();
		
	}
}
