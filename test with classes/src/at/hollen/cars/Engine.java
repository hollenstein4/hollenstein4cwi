package at.hollen.cars;

public class Engine {
	public String fuelType;
	public int horsePower;

	public Engine(String fuelType, int horsePower) {
		super();
		this.fuelType = fuelType;
		this.horsePower = horsePower;
	}


	public String getFuelType() {
		return fuelType;
	}


	public int getHorsePower() {
		return horsePower;
	}



}
